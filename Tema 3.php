<?php
define("TAMANO_MATRIZ", 3);

function generarMatrizAleatoria($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        $fila = array();
        for ($j = 0; $j < $tamano; $j++) {
            $fila[] = rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

function imprimirMatrizTabla($matriz) {
    echo "<table border='1'>";
    foreach ($matriz as $fila) {
        echo "<tr>";
        foreach ($fila as $valor) {
            echo "<td>" . $valor . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>"
}

function sumaDiagonalPrincipal($matriz) {
    $suma = 0;
    for ($i = 0; $i < count($matriz); $i++) {
        $suma += $matriz[$i][$i];
    }
    return $suma;
}

function main() {
    do {
        $matriz = generarMatrizAleatoria(TAMANO_MATRIZ);
        imprimirMatrizTabla($matriz);
        $sumaDiagonal = sumaDiagonalPrincipal($matriz);
        echo "La suma de la diagonal principal es: $sumaDiagonal\n";
        
        if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
            echo "La suma está en el rango de 10 a 15. El script ha terminado.\n";
            break;
        } else {
            echo "La suma no está en el rango de 10 a 15. Generando una nueva matriz...\n";
        }
    echo "<br>";
    } while (true);
}

main();
?>
